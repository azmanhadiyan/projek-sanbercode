<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', function(){
    return redirect('/home');
});

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/home', 'HomeController@store');
Route::post('/home/{id}', 'HomeController@tambah_komentar');
Route::get('/detail/{id}', 'PostController@detail');
Route::post('/detail/{id}', 'PostController@tambah_komentar');
Route::get('/like/{id}', 'PostController@like_post');
Route::get('/like_beranda/{id}', 'HomeController@like_post');
Route::get('/like_comment/{post}/{id}', 'PostController@like_comment');
Route::get('/like_comment/{id}', 'HomeController@like_comment');
Route::get('/post/{post}/{id}/edit', 'PostController@edit_comment');
Route::delete('/post/{post}/{id}/delete', 'PostController@delete_comment');
Route::put('/post/{post}/{id}', 'PostController@update_comment');
Route::get('/profil', 'ProfilController@show_profil');
Route::get('/profil/{id}/edit','ProfilController@edit_profil');
Route::put('/profil/{id}','ProfilController@update_profil');
Route::post('/profil', 'ProfilController@tambah_post' );
Route::post('/profil/{id}', 'ProfilController@tambah_komentar');
Route::get('/like_post_profil/{id}', 'ProfilController@like_post');
Route::get('/like_comment_profil/{id}', 'ProfilController@like_comment');
Route::post('/search', 'HomeController@search');
Route::get('/follow/{id}', 'HomeController@follow');
Route::delete('/profil_post/{id}/delete', 'ProfilController@destroy');
Route::get('/profil_post/{id}/edit', 'ProfilController@edit_post');
Route::put('/profil_post/{id}', 'ProfilController@update_post');
Route::get('/profil_user/{id}', 'ProfilController@get_other_user');
Route::get('/followprofil_user/{id}', 'ProfilController@follow');
Route::post('/comment_other_profil/{post}/{user}', 'ProfilController@comment_other_profil');
Route::get('/like_post_other_profil/{post}/{user}', 'ProfilController@like_other_post');
Route::get('/like_comment_other_profil/{comment}/{user}', 'ProfilController@like_other_comment');


Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web', 'auth']], function () {
    \UniSharp\LaravelFilemanager\Lfm::routes();
});

