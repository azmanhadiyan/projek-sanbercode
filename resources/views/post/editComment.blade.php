@extends('layout.master')

@section('content')
<div class="col-2">
</div>
<div class="col-1"></div>
<div class="col-6">
    <br>
    <div class="card card-warning">
        <div class="card-header">
        <h3 class="card-title">Buat Postingan</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form role="form" action="/post/{{$post}}/{{$comment->id}}" method="POST">
            @csrf
            @method('PUT')
        <div class="card-body">
            <div class="user-block">
                <img class="img-fluid img-circle img-sm" src="{{asset('adminlte/upload/'.$user->avatar)}}" alt="Alt Text">
                <span class="username"><a href="#">{{$user->name}}</a></span>
            </div>
            <div class="form-group">
                <br><br>
                <textarea name="isi" class="form-control ">{!! old('isi', $comment->isi) !!}</textarea>

            </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
        </div>
        </form>
    </div>
</div>
@error('isi')
     <div class="alert alert-danger">{{ $message }}</div>
@enderror
@endsection
